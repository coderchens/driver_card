function getRes(num,resStr) {
  let flag = false
  switch (num) {
    case 1:
      if(resStr === 'A或者正确') {
        flag = true
      }else {
        flag = false
      }
      break;
    case 2:
      if(resStr === 'B或者错误') {
        flag = true
      }else {
        flag = false
      }
      break;
    case 3:
      if(resStr === 'C') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 4:
      if(resStr === 'D') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 7:
      if(resStr === 'AB') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 8:
      if(resStr === 'AC') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 9:
      if(resStr === 'AD') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 10:
      if(resStr === 'BC') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 11:
      if(resStr === 'BD') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 12:
      if(resStr === 'CD') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 13:
      if(resStr === 'ABC') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 14:
      if(resStr === 'ABD') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 15:
      if(resStr === 'ACD') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    case 16:
      if(resStr === 'BCD') {
        flag = true
      }else {
        flag = false
      }
      break;
  
    default:
      if(resStr === 'ABCD') {
        flag = true
      }else {
        flag = false
      }
      break;
  }
  return flag
}


export function checkAnwser(myAns,trueAns,leng) {
  let resStr = ''
  if(trueAns > 4) { //如果anwser大于4说明是多选题
    myAns.sort((a,b) => a-b) //重排序
    for(let i of myAns) { //循环判断拼接答案
      if(i === 1) {
        resStr += 'A'
      }else if(i === 2) {
        resStr += 'B'
      }else if(i === 3) {
        resStr += 'C'
      }else {
        resStr += 'D'
      }
    }

    // return resStr === 
  }else if(leng === 2) { //如果长度是2说明是判断题
    if(myAns[0] === 1) {
      resStr = 'A或者正确'
    }else {
      resStr = 'B或者错误'
    }
  }else if(myAns.length === 1) {
    
    switch (myAns[0]) {
      case 1:
        resStr = 'A或者正确'
        break;
      case 2:
        resStr = 'B或者错误'
        break;
      case 3:
        resStr = 'C'
        break;
      
      default :
      resStr = 'D'
        break;
    }
  }
  const res = getRes(parseInt(trueAns),resStr)
  return res ? '正确' : '错误'
}