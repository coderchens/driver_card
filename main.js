import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
    this.splice(index, 1);
    }
};
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
